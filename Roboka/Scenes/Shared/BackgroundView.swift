//
//  BackgroundView.swift
//  Roboka
//
//  Created by Mohamed EL Meseery on 2/1/20.
//  Copyright © 2020 Mohamed EL Meseery. All rights reserved.
//

import SwiftUI

struct BackgroundView: View {
    let backgroundGradient = Gradient(colors: [
        Color(red: 0.082, green: 0.133, blue: 0.255),
        Color(red: 0.227, green: 0.110, blue: 0.357)
    ])
    
    var body: some View {
        LinearGradient(gradient: backgroundGradient, startPoint: .top, endPoint: .bottom)
            .edgesIgnoringSafeArea(.all)
            .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
    }
}

struct BackgroundView_Previews: PreviewProvider {
    static var previews: some View {
        BackgroundView()
    }
}
