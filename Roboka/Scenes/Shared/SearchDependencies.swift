//
//  SearchDependencies.swift
//  Roboka
//
//  Created by Mohamed EL Meseery on 2/3/20.
//  Copyright © 2020 Mohamed EL Meseery. All rights reserved.
//

import Foundation

class SearchDependencies: ObservableObject {
    var searchDataService: RobotSearchDataServiceType
    var searchDataStore: RobotDataStoreType
    
    init(searchDataService: RobotSearchDataServiceType = RobotSearchDataService(),
         searchDataStore: RobotDataStoreType = RobotDataStore()) {
        self.searchDataService = searchDataService
        self.searchDataStore = searchDataStore
    }
}
