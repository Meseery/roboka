//
//  ImageLoader.swift
//  Roboka
//
//  Created by Mohamed EL Meseery on 2/2/20.
//  Copyright © 2020 Mohamed EL Meseery. All rights reserved.
//

import Foundation
import UIKit.UIImage

class RobotSearchHistoryRecordCellViewModel: ObservableObject {
    @Published var robotImage: UIImage = UIImage()

    private var record: RobotSearchHistoryRecord

    init(record: RobotSearchHistoryRecord) {
        self.record = record
        guard let url = URL(string: record.robotImageURL) else { return }
         let task = URLSession.shared.dataTask(with: url) { data, response, error in
             guard let data = data else { return }
             DispatchQueue.main.async {
                self.robotImage = UIImage(data: data) ?? UIImage()
             }
         }
         task.resume()
    }
    
    var recordDate: String {
        return RobotSearchHistoryRecordCellViewModel.dateFormatter.string(from: record.date)
    }
    
    var robotName: String {
        return record.robotName
    }
    
    static var dateFormatter: DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        return dateFormatter
    }
}
