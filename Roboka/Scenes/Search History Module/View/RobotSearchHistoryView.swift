//
//  RobotSearchHistoryView.swift
//  Roboka
//
//  Created by Mohamed EL Meseery on 1/30/20.
//  Copyright © 2020 Mohamed EL Meseery. All rights reserved.
//

import SwiftUI

struct RobotSearchHistoryView: View {

    @FetchRequest(fetchRequest: RobotDataStore.fetchAllRecordsRequest()) var records:FetchedResults<RobotSearchHistoryRecord>
    
    var body: some View {
        VStack {
            List(self.records, id: \.robotName) { record in
                RobotSearchHistoryRecordCell(record: record)
            }
        }
        .navigationBarTitle("Search History")
        .background(BackgroundView())
    }
}
