//
//  RobotSearchHistoryRecordCell.swift
//  Roboka
//
//  Created by Mohamed EL Meseery on 2/1/20.
//  Copyright © 2020 Mohamed EL Meseery. All rights reserved.
//

import SwiftUI
import Combine

struct RobotSearchHistoryRecordCell: View {
    @ObservedObject private var viewModel: RobotSearchHistoryRecordCellViewModel
    
    init(record: RobotSearchHistoryRecord) {
        viewModel = RobotSearchHistoryRecordCellViewModel(record: record)
    }
    
    var body: some View {
        HStack {
            Image(uiImage: viewModel.robotImage)
                .resizable()
                .frame(width: 50, height: 50)
                .clipShape(Circle())
                .overlay(Circle().stroke(Color.gray, lineWidth: 1))
                .padding()
            Text(viewModel.robotName)
                .font(.subheadline)
                .fontWeight(.bold)
                .foregroundColor(.white)
            Spacer()
            Text(viewModel.recordDate)
                .font(.subheadline)
                .fontWeight(.light)
                .foregroundColor(.white)
        }
    }
}




