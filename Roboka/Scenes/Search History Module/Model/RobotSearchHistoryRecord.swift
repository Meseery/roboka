//
//  RobotSearchResult.swift
//  Roboka
//
//  Created by Mohamed EL Meseery on 1/31/20.
//  Copyright © 2020 Mohamed EL Meseery. All rights reserved.
//

import Foundation
import UIKit.UIImage
import CoreData

class RobotSearchHistoryRecord: NSManagedObject {
    @NSManaged var robotName: String
    @NSManaged var robotImageURL: String
    @NSManaged var date: Date
}
