//
//  SearchBar.swift
//  Roboka
//
//  Created by Mohamed EL Meseery on 1/31/20.
//  Copyright © 2020 Mohamed EL Meseery. All rights reserved.
//

import Foundation
import SwiftUI

struct RobotSearchBarView: View {
    @Binding var searchText: String
    @State private var showCancelButton: Bool = false
    
    var onCommit: (() -> Void)? = nil
    var placeholderText = ""
   
    var body: some View {
        HStack {
            HStack {
                Image(systemName: "magnifyingglass")
                // Search text field
                ZStack (alignment: .leading) {
                    if searchText.isEmpty {
                        Text(placeholderText)
                    }
                    TextField("",
                              text: $searchText,
                              onEditingChanged: { isEditing in
                        self.showCancelButton = true
                    },
                              onCommit: onCommit ?? {})
                        .foregroundColor(.white)
                        .disableAutocorrection(true)
                        .autocapitalization(.none)
                }
                // Clear button
                Button(action: {
                    self.searchText = ""
                }) {
                    Image(systemName: "xmark.circle.fill")
                        .opacity(searchText == "" ? 0 : 1)
                }
            }
            .padding(EdgeInsets(top: 8, leading: 6, bottom: 8, trailing: 6))
            .foregroundColor(.white)
            .background(Color(.tertiarySystemFill))
            .cornerRadius(10.0)
            
            if showCancelButton  {
                // Cancel button
                Button("Cancel") {
                    UIApplication.shared.endEditing(true)
                    self.searchText = ""
                    self.showCancelButton = false
                }
                .foregroundColor(Color(.white))
            }
        }
        .padding(.horizontal)
        .navigationBarHidden(showCancelButton)
    }
}

extension UIApplication {
    func endEditing(_ force: Bool) {
        self.windows
            .filter{$0.isKeyWindow}
            .first?
            .endEditing(force)
    }
}
