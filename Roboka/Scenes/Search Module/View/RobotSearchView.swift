//
//  RobotSearchView.swift
//  Roboka
//
//  Created by Mohamed EL Meseery on 1/30/20.
//  Copyright © 2020 Mohamed EL Meseery. All rights reserved.
//

import SwiftUI

struct RobotSearchView: View {
    @ObservedObject private var viewModel: RobotSearchViewModel = RobotSearchViewModel(robotSearchService: RobotSearchDataService(), robotDataStore: RobotDataStore())
    
    var body: some View {
        ZStack {
            VStack(alignment: .center, spacing: 8) {
                RobotSearchBarView(searchText: $viewModel.searchQuery,
                                   placeholderText: "Get a nice robot icon ...")
                RobotSearchResultCell(robotImage: viewModel.robotImage,
                                      robotName: viewModel.searchQuery,
                                      isLoading: viewModel.isSearching)
                Spacer()
            }
        }
        .alert(isPresented: $viewModel.isError) {
            Alert(title: Text("OoOo"),
                  message: Text(viewModel.error?.localizedDescription ?? "Something went wrong!"),
                  dismissButton: .default(Text("Got it!")))
        }
        .navigationBarTitle("Search Robots")
        .background(BackgroundView())
    }
}
