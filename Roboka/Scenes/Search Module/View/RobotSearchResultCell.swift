//
//  RobotSearchResultCell.swift
//  Roboka
//
//  Created by Mohamed EL Meseery on 1/31/20.
//  Copyright © 2020 Mohamed EL Meseery. All rights reserved.
//

import SwiftUI

struct RobotSearchResultCell: View {
    var robotImage: UIImage? = nil
    var robotName: String? = nil
    var isLoading: Bool = false
    
    var body: some View {
        ZStack(alignment: .center) {
            if isLoading {
                Text("Loading ... ")
                    .foregroundColor(.white)
            }
            Image(uiImage: robotImage ?? UIImage())
                .resizable()
                .clipShape(Circle())
                .frame(width: 180.0,
                       height: 180.0)
                .scaledToFit()
                .overlay(Circle().stroke(Color.white,lineWidth:2).shadow(radius: 20))
        }
        .padding([.top, .bottom], 10)
    }
}
