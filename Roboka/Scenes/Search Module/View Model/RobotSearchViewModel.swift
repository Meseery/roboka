import Foundation
import SwiftUI
import Combine

final class RobotSearchViewModel: ObservableObject {
    @Published var searchQuery = ""
    @Published var isSearching = false
    @Published var isError = false
    @Published private (set) var robotImage: UIImage?
 
    private (set) var error: Error?
    private var robotSearchService: RobotSearchDataServiceType?
    private var robotSearchDataStore: RobotDataStoreType?
    
    init(robotSearchService: RobotSearchDataServiceType, robotDataStore: RobotDataStoreType) {
        self.robotSearchService = robotSearchService
        self.robotSearchDataStore = robotDataStore
        
        searchQueryCancellable = $searchQuery
            .debounce(for: 0.5, scheduler: RunLoop.current)
            .removeDuplicates()
            .map { [weak self] (query) -> String in
                if query.isEmpty {self?.clearSearch()}; return query }
            .filter {!$0.isEmpty && $0.first != " "}
            .sink { [weak self] in
                self?.saveSearchRecord(query:$0)
                self?.performSearch(query: $0)
            }
        print("RobotSearchViewModel init")
    }
    
    private func clearSearch() {
        robotImage = nil
        isSearching = false
    }
    
    private var searchQueryCancellable: Cancellable? {
        didSet {
            oldValue?.cancel()
        }
    }
    
    private var searchCancellable: Cancellable? {
        didSet {
            oldValue?.cancel()
        }
    }

    private func performSearch(query: String) {
        robotImage = nil
        isSearching = true
        searchCancellable = robotSearchService?
            .searchBy(query: query)
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { [weak self] (completion) in
                self?.isSearching = false
                if case let .failure(error) = completion {
                    self?.isError = true
                    self?.error = error
                }
            }, receiveValue: { [weak self](image) in
                self?.robotImage = image
            })
    }
    
    private func saveSearchRecord(query:String) {
        robotSearchDataStore?.save(robotName: query,
                                  robotImageURL:
            APISearchEndpoint.search(query: query).fullURL?.absoluteString ?? "",
                       date: Date())
    }
    
    deinit {
        searchCancellable?.cancel()
        searchQueryCancellable?.cancel()
        print("RobotSearchViewModel deinit")
    }
}

