import Foundation
import Combine

extension APIEndpointType {
    func execute() -> AnyPublisher<Data, Error>  {
        
        guard let url = self.fullURL else {
            return Fail(error: APIError.invalidEndpoint).eraseToAnyPublisher()
        }

        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue

        return URLSession.shared.dataTaskPublisher(for: request)
            .map {$0.data}
            .mapError {
                return APIError.networkError(code: $0.errorCode)
            }
            .eraseToAnyPublisher()
    }
}

extension APIEndpointType {
    var fullURL: URL? {
        var urlComponents = URLComponents()
        urlComponents.scheme = scheme
        urlComponents.host = baseURL
        urlComponents.path = path ?? ""
        if let params = parameters, !params.isEmpty {
            urlComponents.queryItems = [URLQueryItem]()
            for (key, value) in params {
                let queryItem = URLQueryItem(name: key, value: "\(value)")
                urlComponents.queryItems!.append(queryItem)
            }
        }
        return urlComponents.url
    }
}

extension APIEndpointType {
    var parameters: Parameters? { nil }
    var headers: HTTPHeaders? { nil }
    var scheme: String? { "https" }
}
