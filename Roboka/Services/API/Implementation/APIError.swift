import Foundation

extension LocalizedError where Self: CustomStringConvertible {
    var errorDescription: String? {
        return description
    }
}

enum APIError: LocalizedError, CustomStringConvertible {
    case networkError(code: Int)
    case invalidEndpoint
    case networkException
    case invalidData
    
    var description: String {
        switch self {
            case .invalidEndpoint, .networkException:
                return "Something went wrong!"
            case .invalidData:
                return "Unable to fetch results!"
            case let .networkError(errorCode):
                switch errorCode {
                    case 404:
                        return "Sorry, couldn't find data matching your query"
                    default:
                        return "Sorry! unable to serve you this time"
            }
        }
    }
}
