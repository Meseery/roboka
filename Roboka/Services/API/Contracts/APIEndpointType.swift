import Foundation

typealias Parameters = [String: Any]
typealias HTTPHeaders = [String:String]

enum HTTPMethod: String {
    case get = "GET", post = "POST", put = "PUT", patch = "PATCH", delete = "DELETE"
}

protocol APIEndpointType{
    var baseURL: String { get }
    var path: String? { get }
    var method: HTTPMethod { get }
    var parameters: Parameters? { get }
    var headers: HTTPHeaders? { get }
    var scheme: String? { get }
}
