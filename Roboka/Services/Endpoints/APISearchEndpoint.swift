import Foundation

enum APISearchEndpoint: APIEndpointType {
    case search(query: String)
    
    var baseURL: String { "robohash.org" }
    
    var path: String? {
        switch self {
        case .search(let query):
            return "/\(query)"
        }
    }
    
    var method: HTTPMethod { .get }
}
