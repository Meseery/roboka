//
//  RobotSearchLocalDataStore.swift
//  Roboka
//
//  Created by Mohamed EL Meseery on 2/1/20.
//  Copyright © 2020 Mohamed EL Meseery. All rights reserved.
//

import UIKit.UIApplication
import CoreData

class RobotDataStore: RobotDataStoreType {
    private var managedContext: NSManagedObjectContext?
    
    init(managedContext: NSManagedObjectContext? = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext) {
        self.managedContext = managedContext
    }

    func save(robotName: String, robotImageURL: String, date: Date){
        guard let context = managedContext else {return}
        if fetchRecord(robotName: robotName) != nil {
            return
        }
        
        let entity = NSEntityDescription
            .entity(forEntityName: "RobotSearchHistoryRecord",
                    in: context)!
        let record = NSManagedObject(entity:entity, insertInto:context)
        record.setValue(robotName, forKeyPath:"robotName")
        record.setValue(robotImageURL, forKeyPath:"robotImageURL")
        record.setValue(date, forKey: "date")
        
        do {
            try context.save()
        }
        catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    static func fetchAllRecordsRequest() -> NSFetchRequest<RobotSearchHistoryRecord>{
         let request = NSFetchRequest<RobotSearchHistoryRecord>(entityName: "RobotSearchHistoryRecord")
        request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
        return request
    }

    func count() -> Int {
        guard let context = managedContext, let records = try? context.fetch(RobotDataStore.fetchAllRecordsRequest()) else { return 0 }
        return records.count
    }
    
    private func fetchRecord(robotName:String) -> RobotSearchHistoryRecord? {
        guard let context = managedContext else {return nil}
        
        let request = NSFetchRequest<RobotSearchHistoryRecord>(entityName: "RobotSearchHistoryRecord")
        request.predicate = NSPredicate(format: "robotName == %@",robotName)
        
        if let records = try? context.fetch(request) {
            if records.count > 0 {
                return records[0]
            }
        }
        return nil
    }
}
