import Foundation
import UIKit.UIImage
import Combine

class RobotSearchDataService: RobotSearchDataServiceType {
    func searchBy(query: String) -> AnyPublisher<UIImage, Error> {
        let searchEndpoint = APISearchEndpoint.search(query: query)
        return searchEndpoint
            .execute()
            .map {(UIImage(data: $0) ?? UIImage())}
            .eraseToAnyPublisher()
    }
}
