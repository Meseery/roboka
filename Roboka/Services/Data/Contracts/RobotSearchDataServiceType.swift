//
//  RobotSearchDataServiceType.swift
//  Roboka
//
//  Created by Mohamed EL Meseery on 1/31/20.
//  Copyright © 2020 Mohamed EL Meseery. All rights reserved.
//

import Foundation
import Combine
import UIKit.UIImage

protocol RobotSearchDataServiceType: class {
    func searchBy(query: String) -> AnyPublisher<UIImage, Error>
}
