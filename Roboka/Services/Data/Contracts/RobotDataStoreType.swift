//
//  RobotSearchLocalDataServiceType.swift
//  Roboka
//
//  Created by Mohamed EL Meseery on 2/1/20.
//  Copyright © 2020 Mohamed EL Meseery. All rights reserved.
//

import Foundation
import CoreData

protocol RobotDataStoreType: class {
    func save(robotName: String, robotImageURL: String, date: Date)
    func count() -> Int
    static func fetchAllRecordsRequest() -> NSFetchRequest<RobotSearchHistoryRecord>
}
