import SwiftUI

struct ContentView: View {
    @State var searchHistoryCount = 0
    
    var body: some View {
        NavigationView {
            ZStack{
                BackgroundView()
                VStack(alignment: .center) {
                    NavigationLink(
                        "Search Robots",
                        destination:
                        RobotSearchView())
                        .modifier(NavigationButtonModifier(backgroundColor: Color.black, width: 200, height: 60))
                        .padding(.bottom, 20)
                    NavigationLink(
                        "Search History (\(searchHistoryCount))",
                        destination: RobotSearchHistoryView())
                        .modifier(NavigationButtonModifier(backgroundColor: Color.purple, width: 200, height: 60))
                }
            }
            .onAppear {
                self.searchHistoryCount = RobotDataStore().count()
            }
            .navigationBarTitle("Roboka", displayMode: .inline)
        }
        .onAppear {
            UITableView.appearance().backgroundColor = .clear
            UITableViewCell.appearance().backgroundColor = .clear
            UITableView.appearance().tableFooterView = UIView()
            let navigationBarAppearnce = UINavigationBarAppearance()
            navigationBarAppearnce.backButtonAppearance.normal.titleTextAttributes = [.foregroundColor: UIColor.clear]
            navigationBarAppearnce.titleTextAttributes = [.foregroundColor: UIColor.white,
                                                          .font: UIFont.boldSystemFont(ofSize: 24.0)]
            navigationBarAppearnce.configureWithOpaqueBackground()
            navigationBarAppearnce.backgroundColor = .clear
            UINavigationBar.appearance().standardAppearance = navigationBarAppearnce
            UINavigationBar.appearance().tintColor = .white
        }
    }
}

struct NavigationButtonModifier: ViewModifier {
    let backgroundColor: Color
    let textColor: Color? = .white
    let width: CGFloat?
    let height: CGFloat?
    
    func body(content: Content) -> some View {
        content
            .frame(width: width, height: height)
            .background(backgroundColor)
            .cornerRadius(.infinity)
            .foregroundColor(textColor)
    }
}


struct MainView: View {
    var body: some View {
        Form {
            RobotSearchView()
        }
    }
}
