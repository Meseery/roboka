#  Problem

There is a website where we can search/generate robots. The base url is https://robohash.org/ , and appending any string to it we could get a nice robot icon in response. (eg: https://robohash.org/yulia).
Our app should be able to:
- Search for a robot image and display it
- Show the Search History.
### A few conditions which would be good to satisfy:
- We have to be able to search as the user types in a search bar or textfield, without pressing Search button explicitly.
- We have to persist search history in Core Data.
- Ideally the app should have three screens 
    - “Main”, “Search” screen and “Search History” screen.
        The Main screen would have two buttons: “Search” button, which you can tap and go to the “Search” screen, “Search history (count)” button, which you can tap and go see “Search History” list in a separate screen. (Search history button title contains the count, so if the user searched for 5 times in total, the button title would say “Search history (5)“).

All the other details (using libraries or not, library choice, UI details etc.) are totally up to you. Feel free to do it in a way you think is the best. 

# Solution
The solution provided utilize SwiftUI + Combine frameworks upon MVVM architecture which's more suitable than VIPER architecture for this use case since there's no need for over-complexifing the problem. 

![Solution](preview.gif)
