import Foundation
import XCTest
import Combine
@testable import Roboka

class RobotSearchDataServiceTests: XCTestCase {
    var testCancellable: AnyCancellable?
    var sut: RobotSearchDataServiceType?
    
    override func setUp() {
        sut = RobotSearchDataService()
    }

    func test_getRobotIcon() {
        let exp = self.expectation(description: "Getting Robot Icon")
        testCancellable = sut?
            .searchBy(query: "Yul")
            .sink(receiveCompletion: {completion in
                if case let .failure(error) = completion { XCTAssertNotNil(error); exp.fulfill()}
            }, receiveValue: { (image) in
                XCTAssertNotNil(image)
                exp.fulfill()
            })
        waitForExpectations(timeout: 5.0, handler: nil)
    }

    override func tearDown() {
        testCancellable?.cancel()
        sut = nil
    }
}
