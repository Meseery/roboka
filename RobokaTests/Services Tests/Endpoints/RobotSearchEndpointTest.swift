import Foundation
import XCTest
import Combine
@testable import Roboka

class RobotSearchEndpointTest: XCTestCase {
    var cancellable: AnyCancellable?
    
    func test_Search_Endpoint() {
        let searchEndpoint = APISearchEndpoint.search(query: "yuan")
        let exp = self.expectation(description: "Robot Search Endpoint Test")
        cancellable = searchEndpoint
            .execute()
            .sink(receiveCompletion: { (completion) in
                if case let .failure(error) = completion {
                    XCTAssertNotNil(error)
                    exp.fulfill()
                }
            }, receiveValue: { (data) in
                XCTAssertNotNil(data)
                XCTAssertNotNil(UIImage(data: data))
                exp.fulfill()
            })
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    override func tearDown() {
        cancellable?.cancel()
    }
}
