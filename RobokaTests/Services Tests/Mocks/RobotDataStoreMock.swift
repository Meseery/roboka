//
//  RobotSearchLocalDataServiceMock.swift
//  RobokaTests
//
//  Created by Mohamed EL Meseery on 2/3/20.
//  Copyright © 2020 Mohamed EL Meseery. All rights reserved.
//

import Foundation
import CoreData
@testable import Roboka

class RobotDataStoreMock: RobotDataStoreType {
    var savedRecords = [(robotName: String, robotImageURL: String, date: Date)]()
    
    func save(robotName: String, robotImageURL: String, date: Date) {
        savedRecords.append((robotName,robotImageURL,date))
    }
    
    func count() -> Int {
        return savedRecords.count
    }
    
    static func fetchAllRecordsRequest() -> NSFetchRequest<RobotSearchHistoryRecord> {
        NSFetchRequest()
    }
}
