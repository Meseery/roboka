//
//  SearchHistoryRecordViewModelTests.swift
//  RobokaTests
//
//  Created by Mohamed EL Meseery on 2/3/20.
//  Copyright © 2020 Mohamed EL Meseery. All rights reserved.
//

import XCTest
import CoreData
@testable import Roboka

class RobotSearchHistoryRecordCellViewModelTests: XCTestCase {
    var sut: RobotSearchHistoryRecordCellViewModel?
    
    override func setUp() {
        guard let managedObjectContext = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext else { return }
        let record = (NSEntityDescription.insertNewObject(forEntityName: "RobotSearchHistoryRecord", into: managedObjectContext) as! RobotSearchHistoryRecord)
        record.robotName = "can"
        record.robotImageURL = "http://robohash.org/can"
        record.date = Date()
        sut = RobotSearchHistoryRecordCellViewModel(record: record)
    }

    func test_recordViewData() {
        let exp = self.expectation(description: self.debugDescription)
        _ = sut?.$robotImage.sink(receiveValue: { (image) in
            XCTAssertNotNil(image)
            exp.fulfill()
        })
        XCTAssert(sut?.robotName == "can")
        XCTAssert(sut?.recordDate == RobotSearchHistoryRecordCellViewModel.dateFormatter.string(from: Date()))
        wait(for: [exp], timeout: 5.0)
    }
    
    
    override func tearDown() {
        sut = nil
    }

}
