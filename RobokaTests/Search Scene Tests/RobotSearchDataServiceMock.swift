//
//  RobotSearchDataServiceMock.swift
//  RobokaTests
//
//  Created by Mohamed EL Meseery on 2/3/20.
//  Copyright © 2020 Mohamed EL Meseery. All rights reserved.
//

import Foundation
import UIKit.UIImage
import Combine
@testable import Roboka

class RobotSearchDataServiceMock: RobotSearchDataServiceType {
    func searchBy(query: String) -> AnyPublisher<UIImage, Error> {
        let image = UIImage(named: "yuan") ?? UIImage()
        return Future<UIImage, Error> { promise in
            promise(.success(image))
        }.eraseToAnyPublisher()
    }
}
