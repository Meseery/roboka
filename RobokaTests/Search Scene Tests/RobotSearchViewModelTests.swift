//
//  RobotSearchViewModelTests.swift
//  RobokaTests
//
//  Created by Mohamed EL Meseery on 2/3/20.
//  Copyright © 2020 Mohamed EL Meseery. All rights reserved.
//

import XCTest
import Combine
@testable import Roboka

class RobotSearchViewModelTests: XCTestCase {
    var sut: RobotSearchViewModel!
    var searchService: RobotSearchDataServiceType!
    var searchDataStore: RobotDataStoreType!
    var searchCancellable: Cancellable!
    
    override func setUp() {
        searchService =  RobotSearchDataServiceMock()
        searchDataStore = RobotDataStoreMock()
        sut = RobotSearchViewModel(robotSearchService: searchService, robotDataStore: searchDataStore)
    }

    func test_Initialization() {
        XCTAssertNil(sut.robotImage)
        XCTAssertNil(sut.error)
        XCTAssertFalse(sut.isSearching)
        XCTAssertFalse(sut.isError)
        XCTAssert(sut.searchQuery.isEmpty)
    }
    
    func test_Search_Success() {
        let exp = XCTestExpectation(description: self.debugDescription)
        sut.searchQuery = "yuan"
        searchCancellable = sut
            .$robotImage
            .collect(3)
            .sink(receiveValue: { [weak self] (values) in
                guard let `self` = self else { return }
                XCTAssert(values.contains(UIImage(named: "yuan")))
                XCTAssertFalse(self.sut.isError)
                XCTAssertNil(self.sut.error)
                XCTAssertTrue(self.searchDataStore.count() == 1)
                exp.fulfill()
            })
        wait(for: [exp], timeout: 5.0)
    }
    
    override func tearDown() {
        searchCancellable?.cancel()
        sut = nil
    }
}
