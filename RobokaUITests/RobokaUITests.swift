//
//  RobokaUITests.swift
//  RobokaUITests
//
//  Created by Mohamed EL Meseery on 1/30/20.
//  Copyright © 2020 Mohamed EL Meseery. All rights reserved.
//

import XCTest

class RobokaUITests: XCTestCase {
    override func setUp() {
        continueAfterFailure = false
        XCUIApplication().launch()
    }
}
